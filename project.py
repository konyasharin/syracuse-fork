"""
Модуль для CLI
123
234
"""
import argparse


def syracuse_sequence(n):
    """
    :param n: начальное число в сиракузской последовательности
    :return: список чисел сиракузской последовательности
    """
    list_numbers = [n]
    while n != 1:
        if not n % 2:
            n = n // 2
        elif n % 2:
            n = n * 3 + 1
        list_numbers.append(n)
    return list_numbers


def syracuse_max(n):
    """
    :param n: начальное число в сиракузской последовательности
    :return: максимальное число в сиракузской последовательности
    """
    list_numbers = syracuse_sequence(n)
    max_number = list_numbers[0]
    for i in range(len(list_numbers)):
        if list_numbers[i] > max_number:
            max_number = list_numbers[i]
    return max_number


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--command', type=str,
                        help='Введите команду, которую хотите выполнить:\n'
                             'A-вывести сиракузскую последовательность для числа n\n'
                             'B-вывести максимальное число в сиракузской'
                             ' последовательности для числа n\n'
                             'C-выполнить обе команды')
    parser.add_argument('--number', type=int,
                        help='Введите число n-начальное число'
                             ' в сиракузской последовательности')
    args = parser.parse_args()
    if not args.command or not args.number:
        print('ОШИБКА! Введите все входные данные')
    else:
        command_dict = {'A': syracuse_sequence, 'B': syracuse_max}
        if args.command == 'C':
            print(command_dict['A'](args.number), command_dict['B'](args.number), sep='\n')
        else:
            try:
                print(command_dict[args.command](args.number))
            except KeyError:
                print('ОШИБКА! Неверная команда')
